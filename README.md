# CasRx_Sars_Cov2

Optimized CasRx guide RNA design for SARS-COV2


Requirements

python 3.7

numpy 1.9.2

scipy 0.15.1

MDTraj 1.5.0

scikit-learn 0.16.1

matplotlib 1.4.3

GromacsWrapper 0.5.2

mpi4py 2.0.0